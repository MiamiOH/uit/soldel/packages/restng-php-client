<?php


namespace MiamiOH\RESTng\Client;


use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\UriInterface;

class Endpoint
{
    public const DEFAULT_LABEL = 'default';

    /**
     * @var UriInterface
     */
    private $url;
    /**
     * @var string|null
     */
    private $username;
    /**
     * @var string|null
     */
    private $password;
    /**
     * @var string
     */
    private $label;

    /**
     * Endpoint constructor.
     * @param string $url
     * @param string|null $username
     * @param string|null $password
     * @param string $label
     */
    public function __construct(string $url, string $username = null, string $password = null, string $label = self::DEFAULT_LABEL)
    {
        $uriFactory = Psr17FactoryDiscovery::findUriFactory();

        $this->url = $uriFactory->createUri($url);
        $this->username = $username;
        $this->password = $password;
        $this->label = $label;
    }

    public function url(): UriInterface
    {
        return $this->url;
    }

    public function username(): string
    {
        return $this->username;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function label(): string
    {
        return $this->label;
    }

    public function uriTo(UriInterface $uri): UriInterface
    {
        return $uri->withScheme($this->url->getScheme())
            ->withHost($this->url->getHost());
    }

    public function hasCredentials(): bool
    {
        return null !== $this->username;
    }
}
