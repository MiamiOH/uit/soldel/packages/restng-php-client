<?php


namespace MiamiOH\RESTng\Client;


class EndpointCollection
{
    /** @var array */
    private $collection;

    public function __construct()
    {
        $this->collection = [];
    }

    /**
     * @param Endpoint $endPoint
     * @throws RestNgClientException
     */
    public function add(Endpoint $endPoint): void
    {
        if ($this->has($endPoint->label())) {
            throw new RestNgClientException(sprintf('Endpoint with label "%s" was already added', $endPoint->label()));
        }

        $this->collection[$endPoint->label()] = $endPoint;
    }

    /**
     * @param string $label
     * @return bool
     */
    public function has(string $label): bool
    {
        return array_key_exists($label, $this->collection);
    }

    public function get(string $label = Endpoint::DEFAULT_LABEL): Endpoint
    {
        if (!$this->has($label)) {
            throw new RestNgClientException(sprintf('Endpoint with label "%s" was not found', $label));
        }

        return $this->collection[$label];
    }
}