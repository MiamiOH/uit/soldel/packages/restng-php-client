<?php

namespace MiamiOH\RESTng\Client;


use Http\Discovery\HttpClientDiscovery;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class RequestRunner implements LoggerAwareInterface
{
    /** @var ClientInterface */
    private $client;

    /** @var LoggerInterface */
    private $logger;

    /** @var callable|null */
    private $preRequestHook;

    /**
     * RequestRunner constructor.
     * @param ClientInterface|null $client
     */
    public function __construct(ClientInterface $client = null)
    {
        $this->client = $client ?? HttpClientDiscovery::find();

        $this->logger = new NullLogger();
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param callable|null $hook
     * @return $this
     */
    public function withPreRequestHook(?callable $hook): self
    {
        $this->preRequestHook = $hook;

        return $this;
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws RestNgClientException
     */
    public function send(RequestInterface $request): ResponseInterface
    {
        $request = $this->runPreRequestHook($request);

        try {
            $response = $this->client->sendRequest($request);
        } catch (ClientExceptionInterface $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
            throw new RestNgClientException($e->getMessage());
        }

        if ($response->getStatusCode() === 500) {
            $this->logger->error(sprintf('Request for %s failed', $request->getUri()), ['response' => $response]);
            throw new RestNgClientException('REST request failed 500');
        }

        return $response;
    }

    /**
     * @param RequestInterface $request
     * @return RequestInterface
     */
    private function runPreRequestHook(RequestInterface $request): RequestInterface
    {
        if (null === $this->preRequestHook) {
            return $request;
        }

        $this->logger->debug(sprintf('Running pre-request hook for %s', $request->getUri()));
        return call_user_func($this->preRequestHook, $request);
    }
}
