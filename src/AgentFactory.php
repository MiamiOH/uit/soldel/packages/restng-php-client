<?php


namespace MiamiOH\RESTng\Client;


use Http\Discovery\HttpClientDiscovery;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class AgentFactory implements LoggerAwareInterface
{
    /**
     * @var EndpointCollection
     */
    private $endPointCollection;
    /**
     * @var TokenProvider
     */
    private $tokenProvider;

    /** @var RequestRunner  */
    private $runner;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(EndpointCollection $endPointCollection, RequestRunner $runner = null, TokenProvider $tokenProvider = null)
    {
        $this->endPointCollection = $endPointCollection;
        $this->runner = $runner ?? new RequestRunner(HttpClientDiscovery::find());
        $this->tokenProvider = $tokenProvider ?? new TokenProvider($this->runner);

        $this->logger = new NullLogger();
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
        $this->runner->setLogger($logger);
        $this->tokenProvider->setLogger($logger);
    }

    public function makeAgent(string $endPointLabel = Endpoint::DEFAULT_LABEL): Agent
    {
        $agent = new Agent($this->endPointCollection->get($endPointLabel), $this->runner, $this->tokenProvider);

        $agent->setLogger($this->logger);

        return $agent;
    }
}