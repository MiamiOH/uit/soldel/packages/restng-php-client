<?php


namespace MiamiOH\RESTng\Client;


use Carbon\Carbon;

class Token
{
    /** @var string  */
    private $token;
    /** @var Carbon */
    private $expires;

    public function __construct(string $token, Carbon $expires)
    {
        $this->token = $token;
        $this->expires = $expires;
    }

    public function __toString(): string
    {
        return $this->token;
    }

    public function expires(): Carbon
    {
        return $this->expires;
    }

    public function isValid(): bool
    {
        return !empty($this->token) && $this->expires->greaterThan(Carbon::now());
    }
}