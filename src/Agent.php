<?php

namespace MiamiOH\RESTng\Client;


use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class Agent implements LoggerAwareInterface
{
    public const TOKEN_HEADER_FORMAT = 'Token token=%s';

    /** @var Endpoint  */
    private $endPoint;

    /** @var RequestRunner  */
    private $runner;

    /** @var TokenProvider|null  */
    private $tokenProvider;

    /** @var RequestFactoryInterface  */
    private $requestFactory;

    /** @var StreamFactoryInterface  */
    private $streamFactory;

    /** @var LoggerInterface */
    private $logger;

    /** @var ResponseData */
    private $responseData;

    /**
     * RequestRunner constructor.
     * @param Endpoint $endPoint
     * @param RequestRunner|null $runner
     * @param TokenProvider|null $tokenProvider
     */
    public function __construct(Endpoint $endPoint, RequestRunner $runner = null, TokenProvider $tokenProvider = null)
    {
        $this->endPoint = $endPoint;
        $this->runner = $runner ?? new RequestRunner(HttpClientDiscovery::find());
        $this->tokenProvider = $tokenProvider ?? new TokenProvider($runner);

        $this->requestFactory = Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = Psr17FactoryDiscovery::findStreamFactory();

        $this->logger = new NullLogger();
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param string $url
     * @return ResponseData
     * @throws RestNgClientException
     * @throws \JsonException
     */
    public function get(string $url): ResponseData
    {
        $request = $this->requestFactory->createRequest('GET', $url)
            ->withHeader('Accept', 'application/json');

        return $this->run($request);
    }

    /**
     * @param string $url
     * @param array $body
     * @return ResponseData
     * @throws RestNgClientException
     * @throws \JsonException
     */
    public function post(string $url, array $body): ResponseData
    {
        $request = $this->requestFactory->createRequest('POST', $url)
            ->withHeader('Content-type', 'application/json')
            ->withHeader('Accept', 'application/json')
            ->withBody(
                $this->streamFactory->createStream(json_encode($body, JSON_THROW_ON_ERROR))
            );

        return $this->run($request);
    }

    /**
     * @param string $url
     * @param array $body
     * @return ResponseData
     * @throws RestNgClientException
     * @throws \JsonException
     */
    public function put(string $url, array $body): ResponseData
    {
        $request = $this->requestFactory->createRequest('PUT', $url)
            ->withHeader('Content-type', 'application/json')
            ->withHeader('Accept', 'application/json')
            ->withBody(
                $this->streamFactory->createStream(json_encode($body, JSON_THROW_ON_ERROR))
            );

        return $this->run($request);
    }

    /**
     * @param string $url
     * @return ResponseData
     * @throws RestNgClientException
     * @throws \JsonException
     */
    public function delete(string $url): ResponseData
    {
        $request = $this->requestFactory->createRequest('DELETE', $url)
            ->withHeader('Accept', 'application/json');

        return $this->run($request);
    }

    /**
     * @param RequestInterface $request
     * @return ResponseData
     * @throws RestNgClientException
     * @throws \JsonException
     */
    public function run(RequestInterface $request): ResponseData
    {
        $this->logger->debug(sprintf('Running request for %s', $request->getUri()));

        $request = $this->prepareRequest($request);

        $response = $this->send($request);

        $this->responseData = ResponseParser::parse($response, $this->logger);

        $this->logger->debug(sprintf('Response for %s was %s', $request->getUri(), $response->getStatusCode()));

        return $this->responseData;
    }

    /**
     * @param RequestInterface $request
     * @return \Generator
     * @throws RestNgClientException
     * @throws \JsonException
     */
    public function runPaged(RequestInterface $request): \Generator
    {
        $this->run($request);

        yield $this->responseData;

        while ($this->responseData->hasPending()) {
            $pageRequest = $this->prepareRequest($this->nextPageRequest($this->responseData));

            $this->logger->debug(sprintf('Running paged request for %s', $pageRequest->getUri()));
            $this->responseData = ResponseParser::parse($this->send($pageRequest));

            yield $this->responseData;
        }
    }

    /**
     * @param MessageInterface $request
     * @return RequestInterface
     * @throws RestNgClientException
     * @throws \JsonException
     */
    private function prepareRequest(MessageInterface $request): RequestInterface
    {
        if ($this->endPoint->hasCredentials()) {
            $request = $request->withHeader(
                'Authorization',
                $this->makeAuthorizationHeader()
            );
        }

        return $request->withUri($this->endPoint->uriTo($request->getUri()));
    }

    /**
     * @return string
     * @throws RestNgClientException
     * @throws \JsonException
     */
    private function makeAuthorizationHeader(): string
    {
        return sprintf(self::TOKEN_HEADER_FORMAT, $this->tokenProvider->getToken($this->endPoint));
    }

    /**
     * @param ResponseData $responseData
     * @return RequestInterface
     */
    private function nextPageRequest(ResponseData $responseData): RequestInterface
    {
        return $this->requestFactory->createRequest('GET', $responseData->metadataKey('nextUrl'));
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws RestNgClientException
     */
    private function send(RequestInterface $request): ResponseInterface
    {
         $response = $this->runner->send($request);

        if ($response->getStatusCode() === 401) {
            $message = sprintf('User %s is not authorized to access %s', $this->endPoint->username(), $request->getUri());
            $this->logger->error($message);
            throw new RestNgClientException($message);
        }

        return $response;
    }
}
