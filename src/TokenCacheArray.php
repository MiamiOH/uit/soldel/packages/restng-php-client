<?php

namespace MiamiOH\RESTng\Client;


class TokenCacheArray implements TokenCache
{
    private $cache = [];

    public function has(string $key = 'default'): bool
    {
        return array_key_exists($key, $this->cache);
    }

    public function remember(Token $token, string $key = 'default'): void
    {
        $this->cache[$key] = $token;
    }

    public function fetch(string $key = 'default'): Token
    {
        return $this->cache[$key];
    }

    public function forget(string $key = 'default'): void
    {
        if (!$this->has($key)) {
            return;
        }

        unset($this->cache[$key]);
    }
}
