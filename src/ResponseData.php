<?php

namespace MiamiOH\RESTng\Client;


class ResponseData
{
    /**
     * @var int
     */
    private $status;

    /**
     * @var array|null
     */
    private $data;
    /**
     * @var array
     */
    private $metadata;

    /**
     * ResponseData constructor.
     * @param int $status
     * @param array|null $data
     * @param array $metadata
     */
    public function __construct(int $status, array $data = null, array $metadata = [])
    {
        $this->status = $status;
        $this->data = $data;
        $this->metadata = $metadata;
    }

    /**
     * @return int
     */
    public function status(): int
    {
        return $this->status;
    }

    /**
     * @return array|null
     */
    public function data(): ?array
    {
        return $this->data;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->status >= 200 && $this->status < 300;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasMetadataKey(string $key): bool
    {
        return array_key_exists($key, $this->metadata);
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function metadataKey(string $key)
    {
        return $this->metadata[$key] ?? null;
    }

    /**
     * @return array
     */
    public function metadata(): array
    {
        return $this->metadata;
    }

    /**
     * @return bool
     */
    public function hasPending(): bool
    {
        return $this->hasMetadataKey('nextUrl');
    }
}
