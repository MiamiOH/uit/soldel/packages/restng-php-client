<?php


namespace MiamiOH\RESTng\Client;


use Carbon\Carbon;
use GuzzleHttp\Psr7\Uri;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class TokenProvider implements LoggerAwareInterface
{
    public const AUTHENTICATION_RESOURCE_PATH = '/api/authentication/v1';
    public const AUTHENTICATION_TYPE = 'usernamePassword';

    /** @var bool  */
    private static $useCache = false;

    /** @var TokenCache */
    private static $cacheHandler;

    /** @var RequestRunner */
    private $runner;

    /** @var RequestFactoryInterface  */
    private $requestFactory;

    /** @var StreamFactoryInterface  */
    private $streamFactory;

    /** @var LoggerInterface */
    private $logger;

    public static function enableCache(TokenCache $handler = null): void
    {
        if ($handler === null) {
            $handler = new TokenCacheArray();
        }

        self::$cacheHandler = $handler;
        self::$useCache = true;
    }

    public static function disableCache(): void
    {
        self::$cacheHandler = null;
        self::$useCache = false;
    }

    /**
     * TokenProvider constructor.
     * @param RequestRunner|null $runner
     */
    public function __construct(RequestRunner $runner = null)
    {
        $this->runner = $runner ?? new RequestRunner(HttpClientDiscovery::find());

        $this->requestFactory = Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = Psr17FactoryDiscovery::findStreamFactory();

        $this->logger = new NullLogger();
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * @param Endpoint $endPoint
     * @return Token
     * @throws RestNgClientException
     * @throws \JsonException
     */
    public function getToken(Endpoint $endPoint): Token
    {
        if (self::$useCache && self::$cacheHandler->has($endPoint->label())) {
            $token = self::$cacheHandler->fetch($endPoint->label());
            $this->logger->debug(
                sprintf('Retrieved token from cache, expiration: %s', $token->expires()->toW3cString())
            );

            if ($token->isValid()) {
                return $token;
            }

            self::$cacheHandler->forget($endPoint->label());
        }

        $token = $this->requestTokenFromEndpoint($endPoint);

        if (self::$useCache) {
            self::$cacheHandler->remember($token, $endPoint->label());
            $this->logger->debug(sprintf('Saved token to cache, expiration: %s', $token->expires()->toW3cString()));
        }

        return $token;
    }

    /**
     * @param Endpoint $endPoint
     * @return Token
     * @throws RestNgClientException
     * @throws \JsonException
     */
    private function requestTokenFromEndpoint(Endpoint $endPoint): Token
    {
        $body = json_encode([
            'username' => $endPoint->username(),
            'password' => $endPoint->password(),
            'type' => self::AUTHENTICATION_TYPE,
        ], JSON_THROW_ON_ERROR);

        $request = $this->requestFactory->createRequest('post', $endPoint->uriTo(new Uri(self::AUTHENTICATION_RESOURCE_PATH)))
            ->withHeader('Content-Type', 'application/json')
            ->withBody($this->streamFactory->createStream($body));

        $response = $this->runner->send($request);

        if ($response->getStatusCode() === 401) {
            $this->logger->error(
                sprintf('Authentication of token request for %s failed', $endPoint->username()),
                ['response' => $response]
            );
            throw new RestNgClientException(
                sprintf('Failed to authenticate when getting token (username: %s)', $endPoint->username())
            );
        }

        $responseData = ResponseParser::parse($response, $this->logger);

        $data = $responseData->data();

        if (!isset($data['token'])) {
            $this->logger->error(
                sprintf('Invalid token response for %s', $endPoint->username()), ['response-data' => $responseData]
            );
            throw new RestNgClientException(
                sprintf('Invalid token response: no token in response (%s)', $responseData->status())
            );
        }

        return new Token($data['token'], Carbon::parse($data['tokenLifetime']));
    }
}
