<?php

namespace MiamiOH\RESTng\Client;


interface TokenCache
{
    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key = 'default'): bool;

    /**
     * @param Token $token
     * @param string $key
     */
    public function remember(Token $token, string $key = 'default'): void;

    /**
     * @param string $key
     * @return Token
     */
    public function fetch(string $key = 'default'): Token;

    /**
     * @param string $key
     */
    public function forget(string $key = 'default'): void;
}
