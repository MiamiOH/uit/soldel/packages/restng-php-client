<?php

namespace MiamiOH\RESTng\Client;


use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class ResponseParser
{
    /**
     * @param ResponseInterface $response
     * @param LoggerInterface|null $logger
     * @return ResponseData
     * @throws RestNgClientException
     */
    public static function parse(ResponseInterface $response, LoggerInterface $logger = null): ResponseData
    {
        if (null === $logger) {
            $logger = new NullLogger();
        }

        $data = null;

        $metadata = [];

        if (self::hasContent($response) && self::contentIsJson($response)) {
            $logger->debug('Parsing response body as JSON');
            $json = $response->getBody()->getContents();

            try {
                $body = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $e) {
                $logger->error(sprintf('Failed to parse resource response: %s', $e->getMessage()), ['json' => $json]);
                throw new RestNgClientException(sprintf('Failed to parse resource response: %s', $e->getMessage()));
            }

            if (!isset($body['data'])) {
                $logger->error(sprintf('JSON does not contain "data" element'), ['body' => $data]);
                throw new RestNgClientException('Missing element "data" in response');
            }

            $data = $body['data'];

            foreach (array_keys($body) as $key) {
                if ($key === 'data') {
                    continue;
                }

                $metadata[$key] = $body[$key];
            }

        } else if (self::contentIsXml($response)) {
            $logger->debug('Parsing response body as XML');
            $backup = libxml_disable_entity_loader(true);
            $data = json_decode((string)json_encode(simplexml_load_string($response->getBody())), TRUE);
            libxml_disable_entity_loader($backup);

        }

        return new ResponseData(
            $response->getStatusCode(),
            $data,
            $metadata
        );
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    private static function hasContent(ResponseInterface $response): bool
    {
        return $response->getBody()->getSize() > 0;

    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    private static function contentIsJson(ResponseInterface $response): bool
    {
        return $response->hasHeader('content-type')
            && strtolower($response->getHeader('content-type')[0]) === 'application/json';
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    private static function contentIsXml(ResponseInterface $response): bool
    {
        return $response->hasHeader('content-type')
            && strtolower($response->getHeader('content-type')[0]) === 'application/xml';
    }
}
