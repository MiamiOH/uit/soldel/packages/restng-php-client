<?php

namespace Tests;


use MiamiOH\RESTng\Client\ResponseData;

class ResponseDataTest extends TestCase
{

    public function testCanBeCreatedWithStatusAndData(): void
    {
        $this->assertInstanceOf(
            ResponseData::class,
            new ResponseData(200, ['id' => 1])
        );
    }

    public function testCanBeCreatedWithoutData(): void
    {
        $this->assertInstanceOf(
            ResponseData::class,
            new ResponseData(200)
        );
    }

    public function testCanGetStatus(): void
    {
        $response = new ResponseData(200, ['id' => 1]);
        $this->assertEquals(200, $response->status());
    }

    public function testCanGetData(): void
    {
        $data = ['id' => 1];
        $response = new ResponseData(200, $data);
        $this->assertEquals($data, $response->data());
    }

    /**
     * @dataProvider responseStatusProvider
     */
    public function testCanCheckForSuccessStatus(int $status, bool $isSuccess): void
    {
        $response = new ResponseData($status, []);
        $this->assertEquals($isSuccess, $response->isSuccess());
    }

    public function responseStatusProvider(): array
    {
        return [
            '200 Response' => [
                'status' => 200,
                'isSuccess' => true
            ],
            '404 Response' => [
                'status' => 404,
                'isSuccess' => false
            ],
        ];
    }

    /**
     * @dataProvider pagedDataProvider
     */
    public function testCanTellIfHasPendingPagedResponse(array $data, $metadata, $paged): void
    {
        $responseData = new ResponseData(200, $data, $metadata);

        $this->assertEquals($paged, $responseData->hasPending());
    }

    public function pagedDataProvider(): array
    {
        return [
            'No paging' => [
                ['id' => 1, 'name' => 'bob'],
                [],
                false
            ],
            'Paging' => [
                ['id' => 1, 'name' => 'bob'],
                ['nextUrl' => 'https://example.com'],
                true
            ],
        ];
    }

    public function testSetsResponseMetadata(): void
    {
        $responseData = new ResponseData(200, ['id' => 1], ['nextUrl' => 'https://example.com']);

        $this->assertTrue($responseData->hasMetadataKey('nextUrl'));
        $this->assertEquals('https://example.com', $responseData->metadataKey('nextUrl'));
    }

    public function testCanGetAllMetadata(): void
    {
        $responseData = new ResponseData(200, ['id' => 1], ['nextUrl' => 'https://example.com']);

        $this->assertEquals(['nextUrl' => 'https://example.com'], $responseData->metadata());
    }
}
