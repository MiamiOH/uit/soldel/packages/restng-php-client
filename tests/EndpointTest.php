<?php

namespace Tests;

use GuzzleHttp\Psr7\Uri;
use MiamiOH\RESTng\Client\Endpoint;

class EndpointTest extends TestCase
{
    /** @var Endpoint  */
    private $endPoint;

    public function setUp(): void
    {
        parent::setUp();

        $this->endPoint = new Endpoint('https://example.com', 'publicjq', 'sekr3t');
    }

    public function testReturnsUrl(): void
    {
        $this->assertEquals('https://example.com', $this->endPoint->url());
    }

    public function testReturnsUsername(): void
    {
        $this->assertEquals('publicjq', $this->endPoint->username());
    }

    public function testReturnsPassword(): void
    {
        $this->assertEquals('sekr3t', $this->endPoint->password());
    }

    public function testReturnsDefaultLabel(): void
    {
        $this->assertEquals(Endpoint::DEFAULT_LABEL, $this->endPoint->label());
    }

    public function testUsesGivenLabel(): void
    {
        $this->endPoint = new Endpoint('https://example.com', 'publicjq', 'sekr3t', 'test-env');

        $this->assertEquals('test-env', $this->endPoint->label());
    }

    public function testCanMakeUriToResource(): void
    {
        $url = $this->endPoint->uriTo(new Uri('/api/foo?color=blue'));

        $this->assertEquals('https://example.com/api/foo?color=blue', $url);
    }

    /**
     * @dataProvider credentialChecks
     */
    public function testAssertsWhenHasCredentials(EndPoint $endpoint, bool $expected): void
    {
        $this->assertEquals($expected, $endpoint->hasCredentials());
    }

    public function credentialChecks(): array
    {
        return [
            'with credentials' => [new Endpoint('https://example.com', 'publicjq', 'sekr3t'), true],
            'without credentials' => [new Endpoint('https://example.com'), false],
        ];
    }
}
