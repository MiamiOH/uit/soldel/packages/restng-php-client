<?php

namespace Tests;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use MiamiOH\RESTng\Client\Endpoint;
use MiamiOH\RESTng\Client\RequestRunner;
use MiamiOH\RESTng\Client\RestNgClientException;
use MiamiOH\RESTng\Client\Token;
use MiamiOH\RESTng\Client\TokenCache;
use MiamiOH\RESTng\Client\TokenProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\RequestInterface;

class TokenProviderTest extends TestCase
{
    /** @var TokenProvider  */
    private $provider;

    /** @var RequestInterface[] */
    private $requestStack;

    /** @var RequestRunner|MockObject  */
    private $runner;

    /** @var Endpoint  */
    private $endPoint;

    public function setUp(): void
    {
        parent::setUp();

        $this->requestStack = [];

        $this->runner = $this->createMock(RequestRunner::class);

        $this->endPoint = new Endpoint('https://example.com', 'publicjq', 'sekr3t');

        $this->provider = new TokenProvider($this->runner);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        TokenProvider::disableCache();
    }

    public function testMakesAuthenticationPostRequestWithClient(): void
    {
        $this->runner->expects($this->once())->method('send')
            ->with($this->callback(function (RequestInterface $request) {
                $this->requestStack[] = $request;
                return true;
            }))
            ->willReturn($this->newTokenResponse('abc123'));

        $token = $this->provider->getToken($this->endPoint);

        $this->assertEquals('abc123', $token);

        $request = $this->requestStack[0];
        $body = json_decode($request->getBody()->getContents(), true);

        $this->assertEquals($this->endPoint->url() . TokenProvider::AUTHENTICATION_RESOURCE_PATH, $request->getUri());
        $this->assertEquals($this->endPoint->username(), $body['username']);
        $this->assertEquals($this->endPoint->password(), $body['password']);
        $this->assertEquals(TokenProvider::AUTHENTICATION_TYPE, $body['type']);
    }

    public function testThrowsExceptionForInvalidCredentials(): void
    {
        $this->runner->expects($this->once())->method('send')
            ->willReturn(new Response(401));

        $this->expectException(RestNgClientException::class);
        $this->expectExceptionMessage('Failed to authenticate when getting token');

        $this->provider->getToken($this->endPoint);
    }

    public function testThrowsExceptionForEmptyAuthenticationResponse(): void
    {
        $this->runner->expects($this->once())->method('send')
            ->willReturn(new Response(
                200,
                [
                    'content-type' => 'application/json',
                    'content-length' => '0'
                ]
            )
        );

        $this->expectException(RestNgClientException::class);
        $this->expectExceptionMessage('Invalid token response: no token in response');

        $this->provider->getToken($this->endPoint);
    }

    public function testCanSaveTokenDataToCache(): void
    {
        $expectedToken = 'efg456';
        $expires = Carbon::now()->addMinutes(60);

        $this->runner->expects($this->once())->method('send')
            ->willReturn($this->newTokenResponse($expectedToken, $this->endPoint->username(), $expires));

        $cache = $this->createMock(TokenCache::class);

        $cache->expects($this->once())->method('has')
            ->with($this->equalTo($this->endPoint->label()))
            ->willReturn(false);

        $cache->expects($this->once())->method('remember')
            ->with($this->callback(function (Token $token) use ($expectedToken, $expires) {
                $this->assertEquals($expectedToken, $token);
                $this->assertEquals($expires->toW3cString(), $token->expires()->toW3cString());
                return true;
            }));

        TokenProvider::enableCache($cache);

        $this->assertEquals($expectedToken, $this->provider->getToken($this->endPoint));
    }

    public function testCanUseTokenDataFromCache(): void
    {
        $expectedToken = 'efg456';
        $expires = Carbon::now()->addMinutes(60);

        $this->runner->expects($this->never())->method('send');

        $cache = $this->createMock(TokenCache::class);

        $cache->expects($this->once())->method('has')
            ->with($this->equalTo($this->endPoint->label()))
            ->willReturn(true);

        $cache->expects($this->once())->method('fetch')
            ->with($this->equalTo($this->endPoint->label()))
            ->willReturn(new Token($expectedToken, $expires));

        TokenProvider::enableCache($cache);

        $this->assertEquals($expectedToken, $this->provider->getToken($this->endPoint));
    }

    public function testCanGetNewTokenWhenCachedTokenExpires(): void
    {
        $expectedToken = 'efg456';
        $expires = Carbon::now()->addMinutes(60);

        $this->runner->expects($this->once())->method('send')
            ->willReturn($this->newTokenResponse($expectedToken, $this->endPoint->username(), $expires));

        $cache = $this->createMock(TokenCache::class);

        $cache->expects($this->once())->method('has')
            ->with($this->equalTo($this->endPoint->label()))
            ->willReturn(true);

        $cache->expects($this->once())->method('fetch')
            ->with($this->equalTo($this->endPoint->label()))
            ->willReturn(new Token('abc123', Carbon::now()->subHours(2)));

        $cache->expects($this->once())->method('forget')
            ->with($this->equalTo($this->endPoint->label()));

        $cache->expects($this->once())->method('remember')
            ->with($this->callback(function (Token $token) use ($expectedToken, $expires) {
                $this->assertEquals($expectedToken, $token);
                $this->assertEquals($expires->toW3cString(), $token->expires()->toW3cString());
                return true;
            }));

        TokenProvider::enableCache($cache);

        $this->assertEquals($expectedToken, $this->provider->getToken($this->endPoint));
    }

    private function newTokenResponse(string $token = 'abc123', string $username = 'bubba', Carbon $expires = null): Response
    {
        if (null === $expires) {
            $expires = Carbon::now()->addMinutes(60);
        }

        return new Response(
            200,
            [
                'content-type' => 'application/json',
                'content-length' => '10'
            ],
            json_encode([
                'data' => [
                    'token' => $token,
                    'username' => $username,
                    'tokenLifetime' => $expires->toW3cString(),
                ],
                'status' => 200,
                'error' => false,
            ])
        );
    }
}
