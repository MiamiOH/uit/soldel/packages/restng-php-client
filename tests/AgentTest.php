<?php

namespace Tests;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Uri;
use MiamiOH\RESTng\Client\Agent;
use MiamiOH\RESTng\Client\Endpoint;
use MiamiOH\RESTng\Client\RequestRunner;
use MiamiOH\RESTng\Client\ResponseData;
use MiamiOH\RESTng\Client\RestNgClientException;
use MiamiOH\RESTng\Client\Token;
use MiamiOH\RESTng\Client\TokenProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\RequestInterface;

class AgentTest extends TestCase
{
    /** @var Agent  */
    private $agent;

    /** @var RequestRunner|MockObject  */
    private $runner;
    /** @var TokenProvider|MockObject  */
    private $tokenProvider;

    /** @var Endpoint  */
    private $endPoint;

    /** @var Request */
    private $pagedRequest;

    private $pageLimit = 10;
    private $pageTotal = 29;

    public function setUp(): void
    {
        parent::setUp();

        $this->runner = $this->createMock(RequestRunner::class);
        $this->tokenProvider = $this->createMock(TokenProvider::class);

        $this->endPoint = new Endpoint('https://example.com', 'publicjq', 'sekr3t');

        $this->agent = new Agent($this->endPoint, $this->runner, $this->tokenProvider);
    }

    public function testReturnsResponseDataForRequestWithoutAuthorization(): void
    {
        $endPoint = new Endpoint('https://example.com');

        $agent = new Agent($endPoint, $this->runner, $this->tokenProvider);

        $request = new Request('GET', '/path/to/resource?color=blue');

        $this->tokenProvider->expects($this->never())->method('getToken');

        $this->runner->expects($this->once())->method('send')
            ->with($this->callback(function (Request $runRequest) use ($request) {
                $this->assertEquals($this->endPoint->uriTo($request->getUri()), $runRequest->getUri());
                $this->assertFalse($runRequest->hasHeader('Authorization'));
                return true;
            }))
            ->willReturn($this->newJsonResponse(['data' => ['id' => 1]]));

        $responseData = $agent->run($request);

        $this->assertEquals(200, $responseData->status());
        $this->assertEquals(['id' => 1], $responseData->data());
    }

    public function testReturnsResponseDataForRequestWithAuthorization(): void
    {
        $request = new Request('GET', '/path/to/resource?color=blue');
        $token = new Token('abc123', Carbon::now()->addHour());

        $this->tokenProvider->expects($this->once())->method('getToken')
            ->with($this->equalTo($this->endPoint))
            ->willReturn($token);

        $this->runner->expects($this->once())->method('send')
            ->with($this->callback(function (Request $runRequest) use ($request) {
                $this->assertEquals($this->endPoint->uriTo($request->getUri()), $runRequest->getUri());
                $this->assertTrue($runRequest->hasHeader('Authorization'));
                $this->assertEquals(['Token token=abc123'], $runRequest->getHeader('Authorization'));
                return true;
            }))
            ->willReturn($this->newJsonResponse(['data' => ['id' => 1]]));

        $responseData = $this->agent->run($request);

        $this->assertEquals(200, $responseData->status());
        $this->assertEquals(['id' => 1], $responseData->data());
    }

    public function testWillReturnNotFoundResponse(): void
    {
        $request = new Request('GET', '/path/to/resource?color=blue');
        $this->runner->expects($this->once())->method('send')
            ->willReturn(new Response(404));

        $responseData = $this->agent->run($request);

        $this->assertEquals(404, $responseData->status());
    }

    public function testThrowsUnauthorizedException(): void
    {
        $request = new Request('GET', '/path/to/resource?color=blue');
        $this->runner->expects($this->once())->method('send')
            ->willReturn(new Response(401));

        $this->expectException(RestNgClientException::class);
        $this->expectExceptionMessage('User publicjq is not authorized to access https://example.com/path/to/resource?color=blue');

        $this->agent->run($request);
    }

    public function testConstructsAndRunsGetRequest(): void
    {
        $this->runner->expects($this->once())->method('send')
            ->with($this->callback(function (Request $runRequest) {
                $this->assertEquals('GET', $runRequest->getMethod());
                $this->assertEquals($this->endPoint->uriTo(new Uri('/api/path')), $runRequest->getUri());
                $this->assertTrue($runRequest->hasHeader('Authorization'));
                $this->assertTrue($runRequest->hasHeader('Accept'));
                $this->assertContains('application/json', $runRequest->getHeader('Accept'));
                return true;
            }))
            ->willReturn($this->newJsonResponse(['data' => ['id' => 1]]));

        $responseData = $this->agent->get('/api/path');

        $this->assertEquals(200, $responseData->status());
        $this->assertEquals(['id' => 1], $responseData->data());
    }

    public function testConstructsAndRunsPostRequest(): void
    {
        $this->runner->expects($this->once())->method('send')
            ->with($this->callback(function (Request $runRequest) {
                $this->assertEquals('POST', $runRequest->getMethod());
                $this->assertEquals($this->endPoint->uriTo(new Uri('/api/path')), $runRequest->getUri());
                $this->assertEquals(json_encode(['name' => 'George']), $runRequest->getBody()->getContents());
                $this->assertTrue($runRequest->hasHeader('Authorization'));
                $this->assertTrue($runRequest->hasHeader('Accept'));
                $this->assertContains('application/json', $runRequest->getHeader('Accept'));
                $this->assertTrue($runRequest->hasHeader('Content-type'));
                $this->assertContains('application/json', $runRequest->getHeader('Content-type'));
                return true;
            }))
            ->willReturn($this->newJsonResponse(['data' => ['id' => 1]], 201));

        $responseData = $this->agent->post('/api/path', ['name' => 'George']);

        $this->assertEquals(201, $responseData->status());
        $this->assertEquals(['id' => 1], $responseData->data());
    }

    public function testConstructsAndRunsPutRequest(): void
    {
        $this->runner->expects($this->once())->method('send')
            ->with($this->callback(function (Request $runRequest) {
                $this->assertEquals('PUT', $runRequest->getMethod());
                $this->assertEquals($this->endPoint->uriTo(new Uri('/api/path')), $runRequest->getUri());
                $this->assertEquals(json_encode(['name' => 'George']), $runRequest->getBody()->getContents());
                $this->assertTrue($runRequest->hasHeader('Authorization'));
                $this->assertTrue($runRequest->hasHeader('Accept'));
                $this->assertContains('application/json', $runRequest->getHeader('Accept'));
                $this->assertTrue($runRequest->hasHeader('Content-type'));
                $this->assertContains('application/json', $runRequest->getHeader('Content-type'));
                return true;
            }))
            ->willReturn($this->newJsonResponse(['data' => []]));

        $responseData = $this->agent->put('/api/path', ['name' => 'George']);

        $this->assertEquals(200, $responseData->status());
        $this->assertEquals([], $responseData->data());
    }

    public function testConstructsAndRunsDeleteRequest(): void
    {
        $this->runner->expects($this->once())->method('send')
            ->with($this->callback(function (Request $runRequest) {
                $this->assertEquals('DELETE', $runRequest->getMethod());
                $this->assertEquals($this->endPoint->uriTo(new Uri('/api/path')), $runRequest->getUri());
                $this->assertTrue($runRequest->hasHeader('Authorization'));
                $this->assertTrue($runRequest->hasHeader('Accept'));
                $this->assertContains('application/json', $runRequest->getHeader('Accept'));
                return true;
            }))
            ->willReturn($this->newJsonResponse(['data' => []]));

        $responseData = $this->agent->delete('/api/path');

        $this->assertEquals(200, $responseData->status());
    }

    public function testCanIterateOverPagedResource(): void
    {
        $this->pageTotal = 29;
        $this->pageLimit = 10;

        $this->pagedRequest = new Request('GET', '/path/to/resource?color=blue');
        $token = new Token('abc123', Carbon::now()->addHour());

        $this->tokenProvider->expects($this->exactly(3))->method('getToken')
            ->with($this->equalTo($this->endPoint))
            ->willReturn($token);

        /** @var RequestInterface[] $requests */
        $requests = [];
        $this->runner->expects($this->exactly(3))->method('send')
            ->with($this->callback(function (Request $runRequest) use (&$requests) {
                $requests[] = $runRequest;
                $this->assertTrue($runRequest->hasHeader('Authorization'));
                $this->assertEquals(['Token token=abc123'], $runRequest->getHeader('Authorization'));
                return true;
            }))
            ->will($this->onConsecutiveCalls(
                $this->newPagedJsonResponse(1),
                $this->newPagedJsonResponse(11),
                $this->newPagedJsonResponse(21)
            ));

        /** @var ResponseData[] $responses */
        $responses = [];
        foreach ($this->agent->runPaged($this->pagedRequest) as $responseData) {
            $responses[] = $responseData;

            if (count($responses) > ceil($this->pageTotal / $this->pageLimit)) {
                $this->fail('Paged resource requested too many times');
            }
        }

        $this->assertEquals((string) $this->endPoint->uriTo($this->pagedRequest->getUri()), (string) $requests[0]->getUri());
        $this->assertEquals(200, $responses[0]->status());
        $this->assertCount($this->pageLimit, $responses[0]->data());

        $this->assertEquals((string) $this->endPoint->uriTo(new Uri($this->pagedRequest->getUri() . '&limit=10&offset=11')), (string) $requests[1]->getUri());
        $this->assertEquals(200, $responses[1]->status());
        $this->assertCount($this->pageLimit, $responses[1]->data());

        $this->assertEquals((string) $this->endPoint->uriTo(new Uri($this->pagedRequest->getUri() . '&limit=10&offset=21')), (string) $requests[2]->getUri());
        $this->assertEquals(200, $responses[2]->status());
        $this->assertCount(9, $responses[2]->data());
    }

    protected function newPagedJsonResponse(int $offset): Response
    {
        $records = [];
        for ($i = $offset, $max = $offset + $this->pageLimit; $i < $max && $i <= $this->pageTotal; $i++) {
            $records[] = [
                'id' => $offset,
                'key' => md5($offset . $this->pageTotal . $this->pageLimit)
            ];
        }

        $urls = $this->makePagedResponseUrls($offset);

        $body = json_encode(array_merge(
            [
                'data' => $records,
                'status' => 200,
                'error' => false,
                'total' => $this->pageTotal,
            ],
            $urls
        ), JSON_THROW_ON_ERROR);

        $headers = [];
        $headers['content-type'] = 'application/json';
        $headers['content-length'] = strlen($body);

        return new Response(
            200,
            $headers,
            $body
        );

    }

    protected function makePagedResponseUrls(int $offset): array
    {
        $baseUrl = $this->pagedRequest->getUri();

        if (strpos($baseUrl, '?') === false) {
            $baseUrl .= '?';
        } else {
            $baseUrl .= '&';
        }

        $baseUrl .= 'limit=' . $this->pageLimit;

        $urls = [
            'currentUrl' => $baseUrl . '&offset=' . $offset
        ];

        if ($offset + $this->pageLimit < $this->pageTotal) {
            $urls['nextUrl'] = $baseUrl . '&offset=' . ($offset + $this->pageLimit);
        }

        return $urls;
    }
}
