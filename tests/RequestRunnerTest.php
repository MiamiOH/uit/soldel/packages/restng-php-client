<?php

namespace Tests;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use MiamiOH\RESTng\Client\RestNgClientException;
use MiamiOH\RESTng\Client\RequestRunner;
use Psr\Http\Message\RequestInterface;

class RequestRunnerTest extends TestCase
{
    public function testReturnsResponseForRequest(): void
    {
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse(['id' => 1])
        ]);

        $runner = new RequestRunner($client);
        $request = new Request('GET', '/path/to/resource?color=blue');

        $response = $runner->send($request);

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertCount(1, $this->container);

        /** @var RequestInterface $runRequest */
        $runRequest = $this->container[0]['request'];

        $this->assertEquals($request->getUri(), $runRequest->getUri());
    }

    public function testThrowsServerException(): void
    {
        $client = $this->newHttpClientWithResponses([
            new Response(500)
        ]);

        $runner = new RequestRunner($client);
        $request = new Request('GET', '/path/to/resource?color=blue');

        $this->expectException(RestNgClientException::class);
        $runner->send($request);

        $this->assertCount(2, $this->container);
    }

    public function testCallsRegisteredPreRequestHook(): void
    {
        $data = ['id' => 1];

        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse(['data' => $data])
        ]);

        $runner = new RequestRunner($client);
        $hookData = new \stdClass();
        $hookData->hasBeenCalled = false;

        $runner->withPreRequestHook(function(RequestInterface $request) use ($hookData) {
            $hookData->hasBeenCalled = true;
            return $request;
        });

        $request = new Request('GET', '/path/to/resource');

        $runner->send($request);

        $this->assertTrue($hookData->hasBeenCalled);
    }
}
