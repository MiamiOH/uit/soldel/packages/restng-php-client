<?php

namespace Tests;


use Carbon\Carbon;
use GuzzleHttp\Psr7\Response;
use MiamiOH\RESTng\Client\Exceptions\InvalidJsonException;
use MiamiOH\RESTng\Client\RestNgClientException;
use MiamiOH\RESTng\Client\ResponseParser;

class ResponseParserTest extends TestCase
{
    public function testCanParseResponseBodyWithJson(): void
    {
        $data = [
            'token' => 'abc123',
            'username' => 'doej',
            'tokenLifetime' => Carbon::now()->addMinutes(60)->toW3cString(),
        ];

        $response = new Response(
            200,
            [
                'content-type' => 'application/json',
                'content-length' => '10'
            ],
            json_encode([
                'data' => $data,
                'status' => 200,
                'error' => false,
            ])
        );

        $responseData = ResponseParser::parse($response);

        $this->assertEquals(200, $responseData->status());
        $this->assertEquals($data, $responseData->data());
    }

    public function testRequiresDataElementInBody(): void
    {
        $response = new Response(200, [
                'content-type' => 'application/json',
                'content-length' => '10'
            ], json_encode([
                'stuff' => ['id' => 1],
                'status' => 200,
                'error' => false,
            ], JSON_THROW_ON_ERROR));

        $this->expectException(RestNgClientException::class);
        $this->expectExceptionMessage('Missing element "data" in response');

        ResponseParser::parse($response);
    }

    public function testThrowsExceptionForInvalidJson(): void
    {
        $response = new Response(
            200,
            [
                'content-type' => 'application/json',
                'content-length' => '10'
            ],
            'this is not json'
        );

        $this->expectException(RestNgClientException::class);
        $this->expectExceptionMessage('Failed to parse resource response: ');

        ResponseParser::parse($response);
    }

    public function testCanParseEmptyContent(): void
    {
        $response = new Response(
            200,
            [
                'content-type' => 'application/json',
                'content-length' => '0'
            ]
        );

        $responseData = ResponseParser::parse($response);

        $this->assertEquals(200, $responseData->status());
        $this->assertNull($responseData->data());
    }

    public function testCanParseResponseBodyWithXml(): void
    {
        $data = <<<XML
<?xml version='1.0' standalone='yes'?>
<test>
<test-content>
    <firstName>Bill</firstName>
    <lastName>Evans</lastName>
    <year>Junior</year>
</test-content>
</test>
XML;

        $response = new Response(
            200,
            [
                'content-type' => 'application/xml'
            ],
            $data
        );

        $responseData = ResponseParser::parse($response);

        $dataArray = json_decode((string)json_encode(simplexml_load_string($data)), TRUE);

        $this->assertEquals(200, $responseData->status());
        $this->assertEquals($dataArray, $responseData->data());
    }

    public function testCanSetResponseMetadata(): void
    {
        $response = new Response(
            200,
            [
                'content-type' => 'application/json',
                'content-length' => '10'
            ],
            json_encode([
                'data' => ['id' => 1],
                'status' => 200,
                'error' => false,
                'nextUrl' => 'https://example.com'
            ])
        );

        $responseData = ResponseParser::parse($response);

        $this->assertTrue($responseData->hasMetadataKey('nextUrl'));
        $this->assertEquals('https://example.com', $responseData->metadataKey('nextUrl'));
    }

}
