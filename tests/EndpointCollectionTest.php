<?php

namespace Tests;

use MiamiOH\RESTng\Client\Endpoint;
use MiamiOH\RESTng\Client\EndpointCollection;
use MiamiOH\RESTng\Client\RestNgClientException;
use PHPUnit\Framework\TestCase;

class EndpointCollectionTest extends TestCase
{
    /** @var EndpointCollection  */
    private $collection;

    public function setUp(): void
    {
        parent::setUp();

        $this->collection = new EndpointCollection();
    }

    public function testCanAddEndpoint(): void
    {
        $endPoint = new Endpoint('https://example.com', 'publicjq', 'sekr3t');

        $this->collection->add($endPoint);

        $this->assertTrue($this->collection->has(Endpoint::DEFAULT_LABEL));
    }

    public function testCanAddEndpointWithLabel(): void
    {
        $endPoint = new Endpoint('https://example.com', 'publicjq', 'sekr3t', 'test-env');

        $this->collection->add($endPoint);

        $this->assertTrue($this->collection->has('test-env'));
    }

    public function testCannotAddDuplicateLabel(): void
    {
        $endPoint = new Endpoint('https://example.com', 'publicjq', 'sekr3t', 'test-env');

        $this->collection->add($endPoint);

        $this->expectException(RestNgClientException::class);
        $this->expectExceptionMessage('point with label "test-env" was already added');

        $this->collection->add($endPoint);
    }

    public function testGetsDefaultEndpoint(): void
    {
        $endPoint1 = new Endpoint('https://example1.com', 'publicjq', 'sekr3t', 'test-env');
        $endPoint2 = new Endpoint('https://example2.com', 'publicjq', 'sekr3t');

        $this->collection->add($endPoint1);
        $this->collection->add($endPoint2);

        $endPoint = $this->collection->get();

        $this->assertEquals('https://example2.com', $endPoint->url());
    }

    public function testGetsEndpointForSpecificLabel(): void
    {
        $endPoint1 = new Endpoint('https://example1.com', 'publicjq', 'sekr3t', 'test-env');
        $endPoint2 = new Endpoint('https://example2.com', 'publicjq', 'sekr3t');

        $this->collection->add($endPoint1);
        $this->collection->add($endPoint2);

        $endPoint = $this->collection->get('test-env');

        $this->assertEquals('https://example1.com', $endPoint->url());
    }

    public function testThrowsExceptionWhenLabelIsNotFound(): void
    {
        $this->expectException(RestNgClientException::class);
        $this->expectExceptionMessage('Endpoint with label "default" was not found');

        $this->collection->get();
    }
}
