<?php

namespace Tests;

use Carbon\Carbon;
use MiamiOH\RESTng\Client\Token;
use PHPUnit\Framework\TestCase;

class TokenTest extends TestCase
{
    public function testReturnsTokenAsString(): void
    {
        $token = new Token('abc123', Carbon::now()->addHour());

        $this->assertEquals('abc123', $token);
    }

    public function testReturnsExpiration(): void
    {
        $expires = Carbon::now()->addHour();
        $token = new Token('abc123', $expires);

        $this->assertSame($expires, $token->expires());
    }

    public function testAssertsIsValid(): void
    {
        $token = new Token('abc123', Carbon::now()->addHour());

        $this->assertTrue($token->isValid());
    }

    public function testAssertsIsNotValidAfterExpired(): void
    {
        $token = new Token('abc123', Carbon::now()->subHour());

        $this->assertFalse($token->isValid());
    }
}
