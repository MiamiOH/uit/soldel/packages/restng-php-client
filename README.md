# PHP RESTng Client

This package provides a tailored request runner for Miami's RESTng 2 service. It is not intended to be used with any other REST resources (including RESTng 1).

## Installation

**Note** Release 2 of this package is a major rewrite and is not backward compatible.

Add one of the following to your project's `composer.json`.

If installations will be run from internal IT Services systems:

```json
    "repositories": [
        {
            "type": "composer",
            "url": "https://satis.itapps.miamioh.edu/miamioh"
        }
    ]
```

If installations will be run from systems which do not have access to the IT Services satis server:

```json
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/MiamiOH/uit/soldel/packages/restng-php-client.git"
        }
    ]
```

Use `composer` to require the client package:

```shell
composer require miamioh/restng-php-client
```

## Basic Usage

The primary means of running requests is through the `Agent` class. An agent knows what RESTng connection to use through an `Endpoint`. The simplest way to create an `Agent` is:

```php
$agent = new \MiamiOH\RESTng\Client\Agent(
    new \MiamiOH\RESTng\Client\Endpoint($url, $username, $password)
);
```

Note that `Endpoint` URLs should not include a path or end in a trailing slash.

```text
right:

https://ws.apps.miamioh.edu

wrong:

https://ws.apps.miamioh.edu/
https://ws.apps.miamioh.edu/api/...
```

### Creating Requests

The `Agent` runs [PSR-7](https://www.php-fig.org/psr/psr-7/) compliant requests. You can use any supported method to create a request, for example:

```php
$request = new \GuzzleHttp\Psr7\Request(...);

/** @var Psr\Http\Message\RequestFactoryInterface $factory */
$request = $factory->createRequest(...);
```

Note that the URL for the request should be the **relative** path and not include a host. The active `Endpoint` will be used to resolve the absolute URL before the request is sent.

### Running Requests

The `Agent` provides two methods to run a request. The `run()` method runs the request and returns a `ResponseData` object. The `runPaged()` method runs the request and returns a PHP Generator which can be used to iterate over paged `ResponseData` objects.

```php
$responseData = $agent->run($request);

foreach ($agent->runPaged($request) as $responseData) {
    // ...
}
```

### Request Helper Methods

The `Agent` provides helpers for the primary HTTP request methods `GET`, `POST`, `PUT` and `DELETE`.

```php
$responseData = $agent->get('/path/to/resource');
$responseData = $agent->post('/path/to/resource', ['key' => 'value']);
$responseData = $agent->put('/path/to/resource', ['key' => 'value']);
$responseData = $agent->delete('/path/to/resource');
```

### Using ResponseData

The `ResponseData` class provides methods to get the response status, check for success, get the data and get metadata.

```php
// return boolean if status code is in success range
if ($responseData->isSuccess()) {...}

// returns status code as int
$status = $responseData->status();

// returns response body as array
// may be null if response had no body content
$data = $responseData->data();

$metadata = $responseData->metadata();
$value = $responseData->metadataKey($key);
if ($responseData->hasMetadataKey($key)) {...}
```

## Token Cache

The `TokenProvider` class will resolve a new `Token` for the specified `Endpoint`. It will use the Authentication web service located at the `Endpoint` url. Since tokens are good for one hour, you should cache them rather than requesting a new token for each request. This is especially important for long running jobs which make many requests. You can provide a `TokenCache` implementation to the `TokenProvider` and the provider will handle caching and token expiration on your behalf. A `TokenCacheArray` implementation is provided in this package which is suitable for long running processes. You should provide a more appropriate implementation which does not rely on process memory for web applications or short-lived processes.

```php
// Uses TokenCacheArray by default
\MiamiOH\RESTng\Client\TokenProvider::enableCache();

// MyTokenCache must implement TokenCache
\MiamiOH\RESTng\Client\TokenProvider::enableCache(new MyTokenCache());
```

## Multiple Endpoints

While many applications may use a single endpoint (URL, username and password), it is possible to make multiple endpoints available to your application using an `EndpointCollection` and `AgentFactory`.

```php
$endPoints = \MiamiOH\RESTng\Client\new EndpointCollection();

$endPoints->add(new \MiamiOH\RESTng\Client\Endpoint($url, $username, $password, $label));

$agentFactory = new \MiamiOH\RESTng\Client\AgentFactory($endPoints);

// ...

$agent = $agentFactory->makeAgent($label);
```

## Laravel Integration

There are a number of ways this package can be used in a Laravel application. The following are examples which can be used via a `ServiceProvider`:

```php
// Enable caching
TokenProvider::enableCache(new TokenCacheArray());

// Bind an Endpoint to be given when resolving an Agent
$this->app->bind(Endpoint::class, function () {
    $endPointConfig = config('restng.endpoints.' . config('restng.default-endpoint'));

    return new Endpoint(
        $endPointConfig['url'],
        $endPointConfig['username'],
        $endPointConfig['password'],
    );
});

// Bind an Agent with an Endpoint and give it the app log
$this->app->bind(Agent::class, function () {
    $endPointConfig = config('restng.endpoints.' . config('restng.default-endpoint'));

    $endPoint = new Endpoint(
        $endPointConfig['url'],
        $endPointConfig['username'],
        $endPointConfig['password'],
    );

    $agent = new Agent($endPoint);
    
    $agent->setLogger(Log::getLogger());
    
    return $agent;
});

// Bind a populated EndpointCollection
$this->app->bind(EndpointCollection::class, function () {
    $collection = new EndpointCollection();

    foreach (config('restng.endpoints') as $label => $endPointConfig) {
        $collection->add(new Endpoint(
            $endPointConfig['url'],
            $endPointConfig['username'],
            $endPointConfig['password'],
            $label
        ));
    }

    return $collection;
});

// Bind an AgentFactory with specific dependencies and the app log
$this->app->bind(AgentFactory::class, function () {
    $endPoints = $this->app->make(EndpointCollection::class);
    $runner = $this->app->make(RequestRunner::class);
    $tokenProvider = $this->app->make(TokenProvider::class);

    $factory = new AgentFactory($endPoints, $runner, $tokenProvider);
   
    $factory->setLogger(Log::getLogger());

    return $factory;
});

// Use a factory to resolve arbitrary agents by name
$this->app->bind('restng-agent-production', function () {
    /** @var AgentFactory $factory */
    $factory = $this->app->make(AgentFactory::class);

    return $factory->makeAgent();
});
```
